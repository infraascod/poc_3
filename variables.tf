variable "namespace" {
  description = "The project namespace to use for unique resource naming"
  default     = "poc_test"
  type        = string
}

variable "region" {
  description = "AWS region"
  default     = "us-east-1"
  type        = string
}

variable "key_name" {
  description = "AWS Key pair"
  default     = "us-east-1"
  type        = string
}