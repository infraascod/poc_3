output "public_instance" {
  description = "public_instance"
  value       = module.ec2.public_ip
}

output "private_instance" {
  description = "private_instance"
  value       = module.ec2.private_ip
}
